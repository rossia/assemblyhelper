from flask import Flask, render_template, request, jsonify, redirect, session
from src.serverConf import MYSQL_HOST,MYSQL_USER, MYSQL_PASSWORD, MYSQL_DB, ITEMS_DB
from src.queryFunctions import get_freeFEH_list, get_column_names, get_free_list
from src.mechanicalPartsFunctions import mp_row_values
import mysql.connector
import json
from datetime import datetime
tformat="%d/%m/%Y - %H:%M:%S"

def general_update(request,table,columnName):
    mask=[]
    for name in columnName:
        if name in list(request.form.keys()):
            mask.append(1)
        else:
            mask.append(0)
    print(columnName)
    print(list(request.form.keys()))
    print(list(request.form.values()))
    print(mask)
    print(request.form)
    columnVal=[]
    for idx,key in enumerate(columnName):
        #print(str(idx)+"->"+key+": "+str(mask[idx]))
        if key!="new_serial":
            if mask[idx] == 1:
                #print(request.form[key])
                #columnVal.append(request.form[key])
                if request.form[key]!="" or "Jig" in key or "History" in key:
                    columnVal.append(request.form[key])
                else:
                    columnVal.append("NULL")
            else:
                #print("0")
                columnVal.append('0')
    columnVal.append(request.form["new_serial"])
    print(columnVal)
    conn = mysql.connector.connect(host=MYSQL_HOST, user=MYSQL_USER, password=MYSQL_PASSWORD, database=MYSQL_DB)
    cursor = conn.cursor()
    emsg=""
    if "delete" not in request.form.keys():
        # Update record
        histo=f"{datetime.now().strftime(tformat)} - {session['username']} - Manual Update\n"
        query=f"UPDATE {table} SET {columnName[0]} = '{columnVal[-1]}'"
        for i in range(1,len(columnName)):
            if columnVal[i]=="NULL":
                query+=f", {columnName[i]} = NULL"
            else:
                query+=f", {columnName[i]} = '{columnVal[i]}'"
        if "Jig" in table or "VTRX" in table:
            query+=f" WHERE Serial = '{columnVal[0]}'" 
        else:
            query+=f", History=CONCAT('{histo}',History) WHERE Serial = '{columnVal[0]}'"
        print(query)
    else:
        #Delete Record
        if "FEH" in table:
            allowed=get_freeFEH_list("Left")
            allowed.append(get_freeFEH_list("Right"))
        else:
            allowed=get_free_list(table)
        if columnVal[0] not in allowed:
            emsg=f"{columnVal[0]} could not be deleted, it is inside a module"
        query=f"DELETE FROM {table} WHERE Serial = '{columnVal[0]}'"
        print(query)

    if not emsg:
        cursor.execute(query)
    conn.commit()
    cursor.close()
    conn.close()

    return emsg



def Module_update(request):
    col=get_column_names("PS_Module")
    columnName=[item[0] for item in col]
    mask=[]
    for name in columnName:
        if name in list(request.form.keys()):
            mask.append(1)
        else:
            mask.append(0)
    print(columnName)
    print(list(request.form.keys()))
    print(list(request.form.values()))
    print(mask)
    print(request.form)
    columnVal=[]
    if "new_module" in request.form.keys():
        newMod=True
        columnVal.append(request.form['Name_new'])
    else:
        newMod=False
        columnVal.append(request.form['Name'])    
    
    for idx,key in enumerate(columnName):
        if key!="Name":
            if mask[idx] == 1:
                if "History" in key:
                    if newMod:
                        columnVal.append(f"{datetime.now().strftime(tformat)} - {session['username']} - Manually Created\n")
                    else:
                        histo=f"{datetime.now().strftime(tformat)} - {session['username']} - Manual Update\n"
                        columnVal.append(f"CONCAT(\'{histo}\', History)")
                elif request.form[key]!="" or "Jig" in key:
                    columnVal.append(request.form[key])
                else:
                    columnVal.append("NULL")
            else:
                columnVal.append('0')

    conn = mysql.connector.connect(host=MYSQL_HOST, user=MYSQL_USER, password=MYSQL_PASSWORD, database=MYSQL_DB)
    cursor = conn.cursor()


    emsg=""
    if not newMod:
        # Update record
        print(columnName)
        print(columnVal)
        query=f"UPDATE PS_Module SET {columnName[0]} = '{columnVal[1]}'"
        for i in range(2,len(columnVal)):
            if columnVal[i]=="NULL":
                query+=f", {columnName[i-1]} = NULL"
            elif columnName[i-1]=="History":
                query+=f", {columnName[i-1]} = {columnVal[i]}"
            else:
                query+=f", {columnName[i-1]} = '{columnVal[i]}'"
        query+=f" WHERE Name = '{columnVal[0]}'"
        print(query)
     
        cursor.execute(query)
    else:
        cursor.execute("SELECT * FROM PS_Module WHERE Name = %s", (columnVal[0],))
        existing_module = cursor.fetchone()
        if existing_module:
            emsg=("%s existing Module Name" % columnVal[0])
        else:
            # Insert record
            colstr=", ".join(columnName)
            query=f"INSERT INTO PS_Module ({colstr}) VALUES ('{columnVal[0]}'"
            for i in range(1,len(columnVal)):
                if columnVal[i]=="NULL":
                    query+=f", NULL"
                else:
                    query+=f", '{columnVal[i]}'"
            query+=f")"
            print(query)
            cursor.execute(query)
                
    conn.commit()
    cursor.close()
    conn.close()

    return emsg

def manage_assembly(jig,cname,request):
    conn = mysql.connector.connect(host=MYSQL_HOST, user=MYSQL_USER, password=MYSQL_PASSWORD, database=MYSQL_DB)
    cursor = conn.cursor()
    if (request.form["Type"]=="Load"):
        query=f"UPDATE Jig_{jig} SET "
        mp=[]
        used=[]
        for idx in range(1,len(cname)):
            query=query+f"{cname[idx]} = '{request.form[cname[idx]]}'"
            if idx<len(cname)-1:
                 query=query+", "
            if "Batch" in cname[idx]:
                mp.append(idx)
                if "Spacers" in cname[idx]:
                    used.append(4)
                if "Round" in cname[idx]:
                    used.append(2)
                if "Elongated" in cname[idx]:
                    used.append(1)
                if "Isolators" in cname[idx]:
                    used.append(1)
                if "HVtails" in cname[idx]:
                    used.append(1)
        query=query+f" WHERE Serial='{request.form['Serial']}'"
        cursor.execute(query)
        print("here")
        print(query)
        if "baremodule" in jig:
            histo=f"{datetime.now().strftime(tformat)} - {session['username']} - Created by BareModule Jig\n"
            query=f"INSERT INTO PS_Module (Name, Spacing, History) VALUES ('{request.form['PS_Module']}', {request.form['Spacing']}, '{histo}')"
            cursor.execute(query)
            print(query)
        if len(mp)>0:
            connMP = mysql.connector.connect(host=MYSQL_HOST, user=MYSQL_USER, password=MYSQL_PASSWORD, database=ITEMS_DB)
            cursorMP  = connMP.cursor()
            for ii in range(len(mp)):
                if len(mp)==1:
                    tname=cname[mp[ii]].replace('Batch','')
                else:
                    tname="Inserts"
                bname=request.form[cname[mp[ii]]]
                mpVal=mp_row_values(tname,bname)
                if tname=="Spacers":
                    query=f"UPDATE {tname} SET Used={mpVal[4]+used[ii]}, Tested={mpVal[2]-used[ii]} WHERE Batch='{bname}'"
                else:
                    query=f"UPDATE {tname} SET Used={mpVal[4]+used[ii]} WHERE Batch='{bname}'"
                print(query)
                cursorMP.execute(query)
            connMP.commit()
            cursorMP.close()
            connMP.close()
        
    if (request.form["Type"]=="Complete"):
        query=f"UPDATE Jig_{jig} SET "
        for idx in range(1,len(cname)):
            query=query+f"{cname[idx]}=''"
            if idx<len(cname)-1:
                 query=query+", "
        query=query+f" WHERE Serial='{request.form['Serial']}'"
        print(query)
        cursor.execute(query)
        if "inserts" in jig:
            histo=f"{datetime.now().strftime(tformat)} - {session['username']} - Inserts Glued\n"
            query=f"UPDATE PS_Baseplate SET Inserts=1, Jig_BPinserts='{request.form['Serial']}', History=CONCAT('{histo}',History) WHERE Serial='{request.form['PS_Baseplate']}'"
            cursor.execute(query)
            print(query)
        elif "isolator" in jig:
                histo=f"{datetime.now().strftime(tformat)} - {session['username']} - Isolator Glued\n"
                query=f"UPDATE PS_Baseplate SET Isolator=1, Jig_BPisolator='{request.form['Serial']}', History=CONCAT('{histo}',History) WHERE Serial='{request.form['PS_Baseplate']}'"
                cursor.execute(query)
                print(query)
        elif "baremodule" in jig:
                histo=f"{datetime.now().strftime(tformat)} - {session['username']} - BareModule {request.form['PS_Module']}\n"
                query=f"UPDATE PS_Baseplate SET BareModule=1, Jig_PSbaremodule='{request.form['Serial']}', History=CONCAT('{histo}',History) WHERE Serial='{request.form['PS_Baseplate']}'"
                cursor.execute(query)
                print(query)
                query=f"UPDATE PS_Strip SET BareModule=1, Jig_PSbaremodule='{request.form['Serial']}', History=CONCAT('{histo}',History) WHERE Serial='{request.form['PS_Strip']}'"
                cursor.execute(query)
                print(query)
                query=f"UPDATE PS_MAPSA SET BareModule=1, Jig_PSbaremodule='{request.form['Serial']}', History=CONCAT('{histo}',History) WHERE Serial='{request.form['PS_MAPSA']}'"
                cursor.execute(query)
                print(query)
                histo=f"{datetime.now().strftime(tformat)} - {session['username']} - Strip, MaPSA and BP associated\n"
                query=f"UPDATE PS_Module SET PS_Strip='{request.form['PS_Strip']}', PS_MAPSA='{request.form['PS_MAPSA']}', PS_Baseplate='{request.form['PS_Baseplate']}', History=CONCAT('{histo}',History) WHERE Name='{request.form['PS_Module']}'"
                cursor.execute(query)
                print(query)
        elif "PSbonding" in jig:
                histo=f"{datetime.now().strftime(tformat)} - {session['username']} - Bonding completed\n"
                query=f"UPDATE PS_Module SET Bonded=1, Jig_PSbonding='{request.form['Serial']}', History=CONCAT('{histo}',History) WHERE Name='{request.form['PS_Module']}'"
                cursor.execute(query)
                print(query)
        elif "hybrid" in jig:
                histo=f"{datetime.now().strftime(tformat)} - {session['username']} - Glued to {request.form['PS_Module']}\n"
                query=f"UPDATE PS_FEH SET Glued=1, History=CONCAT('{histo}',History) WHERE Serial='{request.form['PS_FEH_L']}'"
                cursor.execute(query)
                print(query)
                query=f"UPDATE PS_FEH SET Glued=1, History=CONCAT('{histo}',History) WHERE Serial='{request.form['PS_FEH_R']}'"
                cursor.execute(query)
                print(query)
                query=f"UPDATE PS_POH SET Glued=1, History=CONCAT('{histo}',History) WHERE Serial='{request.form['PS_POH']}'"
                cursor.execute(query)
                print(query)
                query=f"UPDATE PS_ROH SET Glued=1, History=CONCAT('{histo}',History) WHERE Serial='{request.form['PS_ROH']}'"
                cursor.execute(query)
                print(query)
                histo=f"{datetime.now().strftime(tformat)} - {session['username']} - FEHs, POH and ROH associated\n"
                query=f"UPDATE PS_Module SET PS_FEH_L='{request.form['PS_FEH_L']}', PS_FEH_R='{request.form['PS_FEH_R']}', PS_POH='{request.form['PS_POH']}', PS_ROH='{request.form['PS_ROH']}', Jig_PShybrid='{request.form['Serial']}', History=CONCAT('{histo}',History)  WHERE Name='{request.form['PS_Module']}'"
                cursor.execute(query)
                print(query)
        elif "sandwich" in jig:
                histo=f"{datetime.now().strftime(tformat)} - {session['username']} - Sandwich completed {request.form['PS_MAPSA']}\n"
                query=f"UPDATE PS_Strip SET Sandwich=1, Jig_PSsandwich='{request.form['Serial']}', History=CONCAT('{histo}',History) WHERE Serial='{request.form['PS_Strip']}'"
                cursor.execute(query)
                print(query)
                histo=f"{datetime.now().strftime(tformat)} - {session['username']} - Sandwich completed {request.form['PS_Strip']}\n"
                query=f"UPDATE PS_MAPSA SET Sandwich=1, Jig_PSsandwich='{request.form['Serial']}', History=CONCAT('{histo}',History) WHERE Serial='{request.form['PS_MAPSA']}'"
                cursor.execute(query)
                print(query)
        elif "hvtail" in jig:
                histo=f"{datetime.now().strftime(tformat)} - {session['username']} - HVTail Glued\n"
                query=f"UPDATE PS_Strip SET HVTailGlue=1, Jig_PSShvtail='{request.form['Serial']}', History=CONCAT('{histo}',History) WHERE Serial='{request.form['PS_Strip']}'"
                cursor.execute(query)
                print(query)
        elif "HVbonding" in jig:
                histo=f"{datetime.now().strftime(tformat)} - {session['username']} - HVTail Bonded/Encaps\n"
                query=f"UPDATE PS_Strip SET HVTailBonded=1, HVTailEncaps=1, Jig_HVbonding='{request.form['Serial']}', History=CONCAT('{histo}',History) WHERE Serial='{request.form['PS_Strip']}'"
                cursor.execute(query)
                print(query)
        elif "encapsulation" in jig:
                histo=f"{datetime.now().strftime(tformat)} - {session['username']} - Encapsulation completed\n"
                query=f"UPDATE PS_Module SET Encapsulated=1, Jig_PSencapsulation='{request.form['Serial']}', History=CONCAT('{histo}',History) WHERE Name='{request.form['PS_Module']}'"
                cursor.execute(query)
                print(query)
        elif "metrology" in jig:
                histo=f"{datetime.now().strftime(tformat)} - {session['username']} - Metrology completed\n"
                query=f"UPDATE PS_Module SET Metrology=1, Shift_X='{request.form['shiftx']}', Shift_Y='{request.form['shifty']}', Rotation='{request.form['rotation']}', Jig_PSmetrology='{request.form['Serial']}', History=CONCAT('{histo}',History) WHERE Name='{request.form['PS_Module']}'"
                cursor.execute(query)
                print(query)


    if (request.form["Type"]=="Cables"):
        bname=request.form['PowerTailsBatch']
        mpVal=mp_row_values('PowerTails',bname)
        connMP = mysql.connector.connect(host=MYSQL_HOST, user=MYSQL_USER, password=MYSQL_PASSWORD, database=ITEMS_DB)
        cursorMP  = connMP.cursor()
        query=f"UPDATE PowerTails SET Used={mpVal[2]+1} WHERE Batch='{bname}'"
        print(query)
        cursorMP.execute(query)
        connMP.commit()
        cursorMP.close()
        connMP.close()
        query=f"UPDATE VTRX SET Used=1 WHERE Serial='{request.form['VTRX']}'"
        cursor.execute(query)
        print(query)
        histo=f"{datetime.now().strftime(tformat)} - {session['username']} - VTRX+ associated\n"
        query=f"UPDATE PS_Module SET VTRX='{request.form['VTRX']}', Cables=1, History=CONCAT('{histo}',History) WHERE Name='{request.form['PS_Module']}'"
        cursor.execute(query)
        print(query)

    if (request.form["Type"]=="Pins"):
        connMP = mysql.connector.connect(host=MYSQL_HOST, user=MYSQL_USER, password=MYSQL_PASSWORD, database=ITEMS_DB)
        cursorMP  = connMP.cursor()
        nObj=int(request.form['used'])
        bname=request.form['PinsBatch']
        mpVal=mp_row_values('Pins',bname)
        query=f"UPDATE Pins SET Used={mpVal[2]+nObj} WHERE Batch='{bname}'"
        print(query)
        cursorMP.execute(query)
        bname=request.form['FastenersBatch']
        mpVal=mp_row_values('Fasteners',bname)
        query=f"UPDATE Fasteners SET Used={mpVal[2]+nObj} WHERE Batch='{bname}'"
        print(query)
        cursorMP.execute(query)
        connMP.commit()
        cursorMP.close()
        connMP.close()
        histo=f"{datetime.now().strftime(tformat)} - {session['username']} - Connectors Locked\n"
        query=f"UPDATE PS_Module SET ConnectorsPins=1, History=CONCAT('{histo}',History) WHERE Name='{request.form['PS_Module']}'"
        cursor.execute(query)
        print(query)
        
    conn.commit()
    cursor.close()
    conn.close()




def mass_load(request):
    msg="some exception raised"
    allowed={
         "AssemblyDB" : ["PS_Baseplate","PS_Strip","PS_FEH","PS_ROH","PS_POH","PS_MAPSA","VTRX"],
         "MechanicalParts" : ["HVtails","Inserts","Isolators","Spacers"]}
    columns_name={
         "AssemblyDB" : {
            "PS_Baseplate" : ["Serial","Batch","History"],
            "PS_POH" : ["Serial","Batch","History"],
            "PS_Strip" : ["Serial","Batch","History"],
            "PS_MAPSA" : ["Serial","Batch","History"],
            "PS_ROH" : ["Serial","Spacing","Type","Batch","History"],
            "PS_FEH" : ["Serial","Spacing","Side","Batch","History"],
            "VTRX" : ["Serial","Length"]
         },
         "MechanicalParts" : {
              "HVtails":["Batch","Quantity"],
              "Inserts":["Batch","Quantity"],
              "Isolators":["Batch","Quantity"],
              "Spacers":["Batch","Quantity"]
         }
    }
    if "DB" not in request.form.keys() and "data" not in request.form.keys():
        msg="The data sent have not the expected format"
    else:
        dbName=request.form['DB']
        data=json.loads(request.form['data'])
        nObj=len(data)
        nDone=0
        nErr=0
        nDup=0
        nNot=0
        conn = mysql.connector.connect(host=MYSQL_HOST, user=MYSQL_USER, password=MYSQL_PASSWORD, database=dbName)
        cursor = conn.cursor()
        for obj in data:
            errFlag=False
            if obj[0] in allowed[dbName]:
                cursor.execute(f"SELECT * FROM {obj[0]} WHERE Serial = '{obj[1]}'")
                existing_module = cursor.fetchone()
                if existing_module:
                    nDup+=1
                else:
                    history=f"{datetime.now().strftime(tformat)} - {session['username']} - Inserted on DB\n"
                    columns = ', '.join(columns_name[dbName][obj[0]])
                    if len(columns_name[dbName][obj[0]])==3:
                        query=f"INSERT INTO {obj[0]} ({columns}) VALUES ('{obj[1]}', '{obj[4]}', '{history}')"
                    elif len(columns_name[dbName][obj[0]])==5:
                        query=f"INSERT INTO {obj[0]} ({columns}) VALUES ('{obj[1]}', {obj[2]}, '{obj[3]}', '{obj[4]}', '{history}')"
                    else:
                        if obj[0]=="VTRX":
                            query=f"INSERT INTO {obj[0]} ({columns}) VALUES ('{obj[1]}', '{obj[2]}')"
                        else:   
                            query=f"INSERT INTO {obj[0]} ({columns}) VALUES ('{obj[1]}', {obj[2]})"
                    try:
                        cursor.execute(query)
                    except:
                        print(f"'{query}' FAILED")
                        nErr+=1
                        errFlag=True
                    if not errFlag:
                        nDone+=1
            else:
                nNot+=1
        conn.commit()
        cursor.close()
        conn.close()
        if nDone==nObj:
            msg=f"{nObj} objects sent for uploading\nALL have been successfully uploaded on DB\n"
        else:
            msg=f"{nObj} objects sent for uploading\n{nDone} have been successfully uploaded on DB\n{nDup} were already on DB\n{nNot} are not of a type which is possible to upload\n{nErr} failed to uploading process"
    return msg

def dbFlag_update(table, column, obj):
    conn = mysql.connector.connect(host=MYSQL_HOST, user=MYSQL_USER, password=MYSQL_PASSWORD, database=MYSQL_DB)
    cursor = conn.cursor()
    if "Module" in table:
        ID="Name"
    elif "Strip" in table or "MAPSA" in table:
        ID="Serial"
    else:
        return False
    query=f"UPDATE {table} SET {column} = 1 WHERE {ID} = '{obj}'"
    try:
        cursor.execute(query)
    except:
        print(f"'{query}' FAILED")
        return False
    conn.commit()
    cursor.close()
    conn.close()
    return True