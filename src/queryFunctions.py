from flask import Flask, render_template, request, jsonify, redirect
import mysql.connector
from src.serverConf import MYSQL_HOST,MYSQL_USER, MYSQL_PASSWORD, MYSQL_DB

filterMap={
    "BPinserts": {"PS_Baseplate": "GoodQC=1 AND Unusable=0 AND Inserts=0 AND Isolator=0 AND BareModule=0"},
    "BPisolator": {"PS_Baseplate": "Unusable=0 AND Inserts=1 AND Isolator=0 AND BareModule=0"},
    "PSbaremodule": {"PS_Baseplate": "Unusable=0 AND Inserts=1 AND Isolator=1 AND BareModule=0", "PS_MAPSA": "Unusable=0 AND Sandwich=1 AND BareModule=0", "PS_Strip": "Unusable=0 AND HVTailGlue=1 AND HVTailBonded=1 AND HVTailEncaps=1 AND Sandwich=1 AND BareModule=0", "PS_Module": "PS_Strip IS NULL AND PS_MAPSA IS NULL AND PS_Baseplate IS NULL"},
    "PSbonding": {"PS_Module": "Bonded=0 AND PS_FEH_L LIKE '%' AND PS_FEH_R LIKE '%' AND PS_POH LIKE '%' AND PS_ROH LIKE '%'"},
    "PShybrid": {"PS_Module": "Metrology=1 AND PS_Strip LIKE '%' AND PS_MAPSA LIKE '%' AND PS_Baseplate LIKE '%' AND PS_FEH_L IS NULL AND PS_FEH_R IS NULL AND PS_POH IS NULL AND PS_ROH IS NULL", "PS_FEH_L": "QC=1 AND Glued=0", "PS_FEH_R": "QC=1 AND Glued=0", "PS_POH": "QC=1 AND Glued=0", "PS_ROH": "QC=1 AND Glued=0"},
    "PSsandwich": {"PS_Strip": "Unusable=0 AND HVTailGlue=1 AND HVTailBonded=1 AND HVTailEncaps=1 AND Sandwich=0 AND BareModule=0", "PS_MAPSA": "Unusable=0 AND QC=1 AND Sandwich=0 AND BareModule=0"},
    "PSShvtail": {"PS_Strip": "Unusable=0 AND QC=1 AND HVTailGlue=0 AND HVTailBonded=0 AND HVTailEncaps=0 AND Sandwich=0 AND BareModule=0"},
    "HVbonding": {"PS_Strip": "Unusable=0 AND HVTailGlue=1 AND HVTailBonded=0 AND HVTailEncaps=0 AND Sandwich=0 AND BareModule=0"},
    "PSmetrology": {"PS_Module": "Metrology=0 AND PS_Strip LIKE '%' AND PS_MAPSA LIKE '%' AND PS_Baseplate LIKE '%' AND PS_FEH_L IS NULL AND PS_FEH_R IS NULL AND PS_POH IS NULL AND PS_ROH IS NULL AND Metrology=0"},
    "PSencapsulation": {"PS_Module": "Encapsulated=0 AND Bonded=1 AND PS_Strip LIKE '%' AND PS_MAPSA LIKE '%' AND PS_Baseplate LIKE '%' AND PS_FEH_L LIKE '%' AND PS_FEH_R LIKE '%' AND PS_POH LIKE '%' AND PS_ROH LIKE '%'"},
    "AttachCables": {"PS_Module": "Bonded=1 AND VTRX IS NULL AND Cables=0", "VTRX": "Used=0 AND Unusable=0"},
    "AttachPins": {"PS_Module": "ConnectorsPins=0"}
}




def fetch_table_data(table_name, column_name=None, filter_value=None):
    # Connect to MySQL
    conn = mysql.connector.connect(host=MYSQL_HOST, user=MYSQL_USER, password=MYSQL_PASSWORD, database=MYSQL_DB)
    cursor = conn.cursor()

    # Construct SQL query
    query = f"SELECT * FROM {table_name}"
    if column_name and filter_value:
        query += f" WHERE {column_name} = '{filter_value}'"

    # Execute MySQL query
    cursor.execute(query)

    # Fetch column names
    column_names = [i[0] for i in cursor.description]

    # Fetch all rows
    rows = cursor.fetchall()

    # Close cursor and connection
    cursor.close()
    conn.close()

    return column_names, rows

def get_table_names():
    # Connect to MySQL
    conn = mysql.connector.connect(host=MYSQL_HOST, user=MYSQL_USER, password=MYSQL_PASSWORD, database=MYSQL_DB)
    cursor = conn.cursor()

    # Get table names
    cursor.execute("SHOW TABLES")
    tables = [table[0] for table in cursor.fetchall()]

    # Close cursor and connection
    cursor.close()
    conn.close()

    return tables

def get_column_names(table_name):
    # Connect to MySQL
    conn = mysql.connector.connect(host=MYSQL_HOST, user=MYSQL_USER, password=MYSQL_PASSWORD, database=MYSQL_DB)
    cursor = conn.cursor()

    # Get column names
    cursor.execute(f"DESCRIBE {table_name}")
    dummy=cursor.fetchall()
    columns = [[column[0],column[1]] for column in dummy]
    # Close cursor and connection
    cursor.close()
    conn.close()

    return columns

def get_row_values(table,serial):
    # Connect to MySQL
    conn = mysql.connector.connect(host=MYSQL_HOST, user=MYSQL_USER, password=MYSQL_PASSWORD, database=MYSQL_DB)
    cursor = conn.cursor()

    # Get column names
    if table=="PS_Module":
        cursor.execute(f"SELECT * FROM {table} WHERE Name='{serial}'")
    else:
        cursor.execute(f"SELECT * FROM {table} WHERE Serial='{serial}'")        
    values = [val for val in cursor.fetchone()]
    # Close cursor and connection
    cursor.close()
    conn.close()

    return values

def get_table_rows(table):
    # Connect to MySQL
    conn = mysql.connector.connect(host=MYSQL_HOST, user=MYSQL_USER, password=MYSQL_PASSWORD, database=MYSQL_DB)
    cursor = conn.cursor()

    # Get column names
    if table=="PS_Module":
        cursor.execute(f"SELECT Name FROM {table}")
    else:
        cursor.execute(f"SELECT Serial FROM {table}")
    serials = [serial[0] for serial in cursor.fetchall()]

    # Close cursor and connection
    cursor.close()
    conn.close()

    return serials

def get_free_list(table):
    # Connect to MySQL
    conn = mysql.connector.connect(host=MYSQL_HOST, user=MYSQL_USER, password=MYSQL_PASSWORD, database=MYSQL_DB)
    cursor = conn.cursor()

    # Get column names
    if "Jig" in table:
        cursor.execute(f"SELECT Serial FROM {table}")
    else:
        cursor.execute(f"SELECT Serial FROM {table} WHERE Serial NOT IN (SELECT {table} FROM PS_Module WHERE {table} LIKE '%')")
    serials = [serial[0] for serial in cursor.fetchall()]

    # Close cursor and connection
    cursor.close()
    conn.close()

    return serials

def get_freeFEH_list(side):
    # Connect to MySQL
    conn = mysql.connector.connect(host=MYSQL_HOST, user=MYSQL_USER, password=MYSQL_PASSWORD, database=MYSQL_DB)
    cursor = conn.cursor()

    # Get column names
    if side=="Left":
        cursor.execute(f"SELECT Serial FROM PS_FEH WHERE Side='{side}' AND Serial NOT IN (SELECT PS_FEH_L FROM PS_Module WHERE PS_FEH_L LIKE 'PS%')")
    if side=="Right":
        cursor.execute(f"SELECT Serial FROM PS_FEH WHERE Side='{side}' AND Serial NOT IN (SELECT PS_FEH_L FROM PS_Module WHERE PS_FEH_R LIKE 'PS%')")
    serials = [serial[0] for serial in cursor.fetchall()]

    # Close cursor and connection
    cursor.close()
    conn.close()

    return serials

def get_table_rows_filtered(table,selection):
    # Connect to MySQL
    conn = mysql.connector.connect(host=MYSQL_HOST, user=MYSQL_USER, password=MYSQL_PASSWORD, database=MYSQL_DB)
    cursor = conn.cursor()

    # Get column names
    if selection:
        if table=="PS_Module":
            cursor.execute(f"SELECT Name FROM {table} WHERE {selection}")
        else:
            cursor.execute(f"SELECT Serial FROM {table} WHERE {selection}")
    else:
        if table=="PS_Module":
            cursor.execute(f"SELECT Name FROM {table}")
        else:
            cursor.execute(f"SELECT Serial FROM {table}")
        
    serials = [serial[0] for serial in cursor.fetchall()]

    # Close cursor and connection
    cursor.close()
    conn.close()

    return serials
