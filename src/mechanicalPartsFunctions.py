from flask import Flask, render_template, request, jsonify, redirect
import mysql.connector
from src.serverConf import MYSQL_HOST,MYSQL_USER, MYSQL_PASSWORD, ITEMS_DB

def mp_table_data(table_name, column_name=None, filter_value=None):
    # Connect to MySQL
    conn = mysql.connector.connect(host=MYSQL_HOST, user=MYSQL_USER, password=MYSQL_PASSWORD, database=ITEMS_DB)
    cursor = conn.cursor()

    # Construct SQL query
    query = f"SELECT * FROM {table_name}"
    if column_name and filter_value:
        query += f" WHERE {column_name} = '{filter_value}'"

    # Execute MySQL query
    cursor.execute(query)

    # Fetch column names
    column_names = [i[0] for i in cursor.description]

    # Fetch all rows
    rows = cursor.fetchall()

    # Close cursor and connection
    cursor.close()
    conn.close()

    return column_names, rows

def mp_table_names():
    # Connect to MySQL
    conn = mysql.connector.connect(host=MYSQL_HOST, user=MYSQL_USER, password=MYSQL_PASSWORD, database=ITEMS_DB)
    cursor = conn.cursor()

    # Get table names
    cursor.execute("SHOW TABLES")
    tables = [table[0] for table in cursor.fetchall()]

    # Close cursor and connection
    cursor.close()
    conn.close()

    return tables

def mp_column_names(table_name):
    # Connect to MySQL
    conn = mysql.connector.connect(host=MYSQL_HOST, user=MYSQL_USER, password=MYSQL_PASSWORD, database=ITEMS_DB)
    cursor = conn.cursor()

    # Get column names
    cursor.execute(f"DESCRIBE {table_name}")
    dummy=cursor.fetchall()
    columns = [[column[0],column[1]] for column in dummy]
    # Close cursor and connection
    cursor.close()
    conn.close()

    return columns

def mp_row_values(table,batch):
    # Connect to MySQL
    conn = mysql.connector.connect(host=MYSQL_HOST, user=MYSQL_USER, password=MYSQL_PASSWORD, database=ITEMS_DB)
    cursor = conn.cursor()

    # Get column names
    cursor.execute(f"SELECT * FROM {table} WHERE Batch='{batch}'")        
    values = [val for val in cursor.fetchone()]
    # Close cursor and connection
    cursor.close()
    conn.close()

    return values

def mp_table_rows(table):
    # Connect to MySQL
    conn = mysql.connector.connect(host=MYSQL_HOST, user=MYSQL_USER, password=MYSQL_PASSWORD, database=ITEMS_DB)
    cursor = conn.cursor()

    # Get column names
    cursor.execute(f"SELECT Batch FROM {table}")
    batches = [batch[0] for batch in cursor.fetchall()]

    # Close cursor and connection
    cursor.close()
    conn.close()

    return batches

def mp_update(request,table,columnName):
    columnVal=[]
    for key in request.form:
        print(key)
        if key!="new_batch":
            columnVal.append(request.form[key])
    columnVal.append(request.form["new_batch"])
    print(columnName)
    print(columnVal)
    conn = mysql.connector.connect(host=MYSQL_HOST, user=MYSQL_USER, password=MYSQL_PASSWORD, database=ITEMS_DB)
    cursor = conn.cursor()
    emsg=""
    if "delete" not in request.form.keys():
        # Update record
        query=f"UPDATE {table} SET {columnName[0]} = '{columnVal[-1]}'"
        for i in range(1,len(columnName)):
            if columnVal[i]=="NULL":
                query+=f", {columnName[i]} = NULL"
            else:
                query+=f", {columnName[i]} = '{columnVal[i]}'"
        query+=f" WHERE Batch = '{columnVal[0]}'"
        print(query)
    else:
        #Delete Record
        query=f"DELETE FROM {table} WHERE Batch = '{columnVal[0]}'"
        print(query)

    if not emsg:
        cursor.execute(query)
    conn.commit()
    cursor.close()
    conn.close()

    return emsg

