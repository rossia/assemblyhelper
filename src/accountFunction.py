from flask import Flask, render_template, request, jsonify, redirect, session
from src.serverConf import MYSQL_HOST,MYSQL_USER, MYSQL_PASSWORD, USERS_DB
import mysql.connector
import re, hashlib


def updatePWD(request,sec_key):   
    username = request.form['username']
    password = request.form['current_pwd']
    email = request.form['email']
    newpwd = request.form['new_pwd']
    if newpwd!=request.form['confirm_pwd']:
        return "The new passwords submitted are not the same"
    if len(newpwd)<6:
        return "The new password is too short, at least six characters"
    hash = password + sec_key
    hash = hashlib.sha1(hash.encode())
    password = hash.hexdigest()
    conn = mysql.connector.connect(host=MYSQL_HOST, user=MYSQL_USER, password=MYSQL_PASSWORD, database=USERS_DB)
    cursor = conn.cursor()
    cursor.execute('SELECT * FROM accounts WHERE username = %s AND password = %s', (username, password,))
    # Fetch one record and return result
    account = cursor.fetchone()
    if account:
        hash = newpwd + sec_key
        hash = hashlib.sha1(hash.encode())
        newpwd = hash.hexdigest()
        cursor.execute(f"UPDATE accounts SET password='{newpwd}' WHERE username='username'")
        conn.commit()
        cursor.close()
        conn.close()
        return "Password successfully changed!"
    else:
        cursor.close()
        conn.close()
        return "Wrong current password!"