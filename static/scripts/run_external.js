// static/scripts/run_external.js

function runExternalScript() {
    var DBinit={{ DBinit | tojson | safe }};
    var cache="{{ cacheFile | tojson | safe }}";
    var user=null
    var pwd=null
    var otp=null
    if (!DBinit){
        // Open popup for name input
        user = prompt("Username:");
        if (user === null) return; // If Cancel is clicked

        // Open popup for age input
        pwd = prompt("Password:");
        if (pwd === null) return; // If Cancel is clicked

        // Open popup for age input
        otp = prompt("Otp:");
        if (otp === null) return; // If Cancel is clicked
    }

    // Send inputs to Flask route for processing
    fetch('/run_external_script', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({ user: user, pwd: pwd, otp: otp, cache= cache}),
    })
    .then(response => response.text())
    .then(result => {
        alert(result); // Show the result in an alert
    })
    .catch(error => {
        console.error('Error:', error);
    });
}
