from flask import Flask, render_template, request, jsonify, redirect, url_for, session
import mysql.connector
from src.queryFunctions import fetch_table_data, get_table_names, get_column_names, get_row_values, get_table_rows, get_free_list, get_freeFEH_list,get_table_rows_filtered,filterMap
from src.updateFunctions import Module_update, general_update, manage_assembly, mass_load, dbFlag_update
from src.mechanicalPartsFunctions import mp_table_data, mp_table_names, mp_column_names, mp_row_values, mp_table_rows, mp_update
from src.accountFunction import updatePWD
from src.serverConf import MYSQL_HOST,MYSQL_USER, MYSQL_PASSWORD, MYSQL_DB, USERS_DB, ITEMS_DB
import re, hashlib
import os
from datetime import datetime, timedelta
import time
#from flask_socketio import SocketIO, emit
import subprocess
import pexpect

MP=["Inserts","Isolators","HVtails","Spacers","PowerTails","LightShields","Pins","Fasteners"]

tformat="%d/%m/%Y - %H:%M:%S"

app = Flask(__name__)
app.secret_key = "4815162342"
app.permanent_session_lifetime = timedelta(minutes=60)
app.session_refresh_each_request = True
#socketio = SocketIO(app)

@app.route('/login', methods=['GET', 'POST'])
@app.route('/', methods=['GET', 'POST'])
def login():
    msg = ''
    if 'loggedin' not in session:
        # Check if "username" and "password" POST requests exist (user submitted form)
        if request.method == 'POST' and 'username' in request.form and 'password' in request.form:
            # Create variables for easy access
            username = request.form['username']
            password = request.form['password']
            hash = password + app.secret_key
            hash = hashlib.sha1(hash.encode())
            password = hash.hexdigest()
            # Check if account exists using MySQL
            conn = mysql.connector.connect(host=MYSQL_HOST, user=MYSQL_USER, password=MYSQL_PASSWORD, database=USERS_DB)
            cursor = conn.cursor()
            cursor.execute('SELECT * FROM accounts WHERE username = %s AND password = %s', (username, password,))
            # Fetch one record and return result
            account = cursor.fetchone()
            cursor.close()
            conn.close()
            # If account exists in accounts table in out database
            if account:
                print("User "+ account[1] + " logged in")
                # Create session data, we can access this data in other routes
                session.permanent=True
                session['loggedin'] = True
                session['id'] = account[0]
                session['username'] = account[1]
                session['email'] = account[2]
                # Redirect to home page
                return redirect('/view')
            else:
                # Account doesnt exist or username/password incorrect
                msg = 'Incorrect username/password!'
        # Show the login form with message (if any)
        return render_template('login.html', msg=msg)
    else:
        return redirect('/view')

@app.route('/logout')
def logout():
    # Remove session data, this will log the user out
    session.pop('loggedin', None)
    session.pop('id', None)
    session.pop('username', None)
    msg="User Logged out"
    # Redirect to login page
    #return redirect(url_for('login',msg=msg))
    return redirect(url_for('login'))

@app.route('/register', methods=['GET', 'POST'])
def register():
    # Output message if something goes wrong...
    msg = ''
    # Check if "username", "password" and "email" POST requests exist (user submitted form)
    if request.method == 'POST' and 'username' in request.form and 'password' in request.form and 'email' in request.form:
        # Create variables for easy access
        username = request.form['username']
        password = request.form['password']
        email = request.form['email']
        conn = mysql.connector.connect(host=MYSQL_HOST, user=MYSQL_USER, password=MYSQL_PASSWORD, database=USERS_DB)
        cursor = conn.cursor()
        cursor.execute('SELECT * FROM accounts WHERE username = %s', (username,))
        account = cursor.fetchone()
        # If account exists show error and validation checks
        if account:
            msg = 'Account already exists!'
        elif not re.match(r'[^@]+@[^@]+\.[^@]+', email):
            msg = 'Invalid email address!'
        elif not re.match(r'[A-Za-z0-9]+', username):
            msg = 'Username must contain only characters and numbers!'
        elif not username or not password or not email:
            msg = 'Please fill out the form!'
        else:
            # Hash the password
            hash = password + app.secret_key
            hash = hashlib.sha1(hash.encode())
            password = hash.hexdigest()
            # Account doesn't exist, and the form data is valid, so insert the new account into the accounts table
            cursor.execute('INSERT INTO accounts VALUES (NULL, %s, %s, %s)', (username, email, password,))
            msg = 'You have successfully registered!'
        conn.commit()
        cursor.close()
        conn.close()
    elif request.method == 'POST':
        # Form is empty... (no POST data)
        msg = 'Please fill out the form!'
    # Show registration form with message (if any)
    return render_template('register.html', msg=msg)

@app.route('/profile',methods=['GET', 'POST'])
def profile():
    if 'loggedin' in session:
        if request.method == 'POST':
            msg=updatePWD(request,app.secret_key)
            return render_template('profile.html',user=session['username'],mail=session['email'],msg=msg)
        else:
            return render_template('profile.html',user=session['username'],mail=session['email'])
    else:
        return redirect('/login')

@app.route('/columns/<table_name>')
def columns(table_name):
    if table_name in MP:
        columns = mp_column_names(table_name)
    else:
        columns = get_column_names(table_name)
    return jsonify(columns)

@app.route('/<table>')
def tables_key(table):
    if table in MP:
        serials = mp_table_rows(table)
    else:
        serials = get_table_rows(table)
    return jsonify(serials)

@app.route('/<table>/<serial>')
def tables_value(table,serial):
    if table in MP:
        serials = mp_row_values(table,serial)
    else:
        serials = get_row_values(table,serial)
    return jsonify(serials)

@app.route('/free/<table>')
def tables_free_key(table):
    if table=="PS_FEH":
        return render_template('freeFEH.html')
    elif table=="PS_Module":
        return redirect("/view")
    else:
        serials = get_free_list(table)
        return jsonify(serials)

@app.route('/free/PS_FEH/<side>')
def feh_free_key(side):
    serials = get_freeFEH_list(side)
    return jsonify(serials)

@app.route('/update')
def updateMenu():
    if 'loggedin' in session:
        return render_template('update_menu.html', user=session['username'])
    else:
        return redirect('/login')

@app.route('/updateJigs')
def updateJigsMenu():
    if 'loggedin' in session:
        return render_template('updateJigs_menu.html', user=session['username'])
    else:
        return redirect('/login')

@app.route('/updateParts')
def updatePartsMenu():
    if 'loggedin' in session:
        return render_template('updateParts_menu.html', user=session['username'])
    else:
        return redirect('/login')

@app.route('/view', methods=['GET', 'POST'])
def index():
    if 'loggedin' in session:
        if request.method == 'POST':
            selected_table = request.form['table']
            selected_column = request.form['column']
            filter_value = request.form['filter_value']
            print(selected_table)
            print(selected_column)
            print(filter_value)
            #MP=["HVtails","Inserts","Isolators","Spacers"]
            if selected_table in MP:
                column_names, rows = mp_table_data(selected_table, selected_column, filter_value)
            else:   
                column_names, rows = fetch_table_data(selected_table, selected_column, filter_value)
            tt=get_table_names()
            mp=mp_table_names()
            tt.extend(mp)
            return render_template('view.html', tables=tt, selected_table=selected_table, selected_column=selected_column, filter_value=filter_value, column_names=column_names, rows=rows,user=session['username'])
        else:
            tt=get_table_names()
            mp=mp_table_names()
            tt.extend(mp)
            return render_template('view.html', tables=tt, selected_table=None,user=session['username'])
    else:
        return redirect('/login')

@app.route('/insert', methods=['GET', 'POST'])
def insert():
    if 'loggedin' in session:
        if request.method == 'POST':
            #MP=["HVtails","Inserts","Isolators","Spacers"]
            # Get form data
            table_name = request.form['table']
            # Assuming you have a form input for each column
            column_values = {key: request.form[key] for key in request.form if key != 'table'}

            # Insert data into the database
            if table_name in MP:
                conn = mysql.connector.connect(host=MYSQL_HOST, user=MYSQL_USER, password=MYSQL_PASSWORD, database=ITEMS_DB)
            else:
                conn = mysql.connector.connect(host=MYSQL_HOST, user=MYSQL_USER, password=MYSQL_PASSWORD, database=MYSQL_DB)
            cursor = conn.cursor()
            if "History" in column_values:
                column_values["History"]=f"{datetime.now().strftime(tformat)} - {session['username']} - Inserted on DB\n"
            columns = ', '.join(column_values.keys())
            values = ', '.join(['%s'] * len(column_values))
            query = f"INSERT INTO {table_name} ({columns}) VALUES ({values})"
            cursor.execute(query, list(column_values.values()))

            conn.commit()
            cursor.close()
            conn.close()

            return redirect('/insert')
        else:
            tt=get_table_names()
            mp=mp_table_names()
            tt.remove("PS_Module")
            tt.extend(mp)
            return render_template('insert.html', tables=tt, user=session['username'])
    else:
        return redirect('login')
    
@app.route('/load', methods=['GET', 'POST'])
def load_from_file():
    if 'loggedin' in session:
        if request.method == 'POST':
            msg=mass_load(request)
            return msg
        else:
            return render_template('load.html', user=session['username'])
    else:
        return redirect('login')
    
@app.route('/module', methods=['GET', 'POST'])
def update_module():
    if 'loggedin' in session:
        if request.method == 'POST':
            emsg=Module_update(request)
            if emsg:
                return render_template('update_module.html',error_message=emsg,user=session['username'])            
            else:
                if "new_module" in request.form.keys():
                    modName=request.form['Name_new']
                else:
                    modName=request.form['Name']
                return redirect('/module/'+modName)
        else:
            return render_template('update_module.html',user=session['username'])
    else:
        return redirect('login')
    
@app.route('/module/<initial_module_name>', methods=['GET', 'POST'])
def update_module2(initial_module_name):
    if 'loggedin' in session:
        if request.method == 'POST':
            emsg=Module_update(request)
            if emsg:
                return render_template('update_module.html',error_message=emsg,user=session['username'])            
            else:
                if "new_module" in request.form.keys():
                    modName=request.form['Name_new']
                else:
                    modName=request.form['Name']
                return redirect('/module/'+modName)
        else:
            return render_template('update_module.html', initial_module_name=initial_module_name,user=session['username'])
    else:
        return redirect('login')
    
@app.route('/update/<table>', methods=['GET', 'POST'])
def updateElement(table):
    if 'loggedin' in session:
        if "PS_Module" in table:
            return redirect('/module')
        else:
            if table in MP:
                col=mp_column_names(table)
                columns_name=[item[0] for item in col]
                columns_type=[item[1] for item in col]
                print(columns_name)
                print(table)
                if request.method == 'POST':
                    emsg=mp_update(request,table,columns_name)
                    if emsg:
                        return render_template('update_MPelement.html',table_name=table,columns_name=columns_name,columns_type=columns_type,error_message=emsg,user=session['username'])            
                    else:
                        return redirect('/update/'+table+'/'+request.form["new_batch"])
                else:
                    return render_template('update_MPelement.html', table_name=table, columns_name=columns_name,columns_type=columns_type,user=session['username'])
            else:
                col=get_column_names(table)
                columns_name=[item[0] for item in col]
                columns_type=[item[1] for item in col]
                print(columns_name)
                print(table)
                jig_dict={}
                for name in columns_name:
                    if "Jig" in name:
                        ll=get_table_rows(name)
                        jig_dict[name]=ll
                print(jig_dict)
                if request.method == 'POST':
                    emsg=general_update(request,table,columns_name)
                    if emsg:
                        return render_template('update_element.html',table_name=table,columns_name=columns_name,columns_type=columns_type, jig_dict=jig_dict,error_message=emsg,user=session['username'])            
                    else:
                        return redirect('/update/'+table+'/'+request.form["new_serial"])
                else:
                    return render_template('update_element.html', table_name=table, columns_name=columns_name,columns_type=columns_type, jig_dict=jig_dict,user=session['username'])
    else:
        return redirect('login')

@app.route('/update/<table>/<initial_name>', methods=['GET', 'POST'])
def updateElement2(table, initial_name):
    if 'loggedin' in session:
        if "PS_Module" in table:
            return redirect('/module/'+initial_name)
        else:
            if table in MP:
                col=mp_column_names(table)
                columns_name=[item[0] for item in col]
                columns_type=[item[1] for item in col]
                print(columns_name)
                print(table)
                if request.method == 'POST':
                    emsg=mp_update(request,table,columns_name)
                    if emsg:
                        return render_template('update_MPelement.html',table_name=table,columns_name=columns_name,columns_type=columns_type,initial_name=initial_name,error_message=emsg,user=session['username'])            
                    else:
                        return redirect('/update/'+table+'/'+request.form["new_batch"])
                else:
                    return render_template('update_MPelement.html', table_name=table, columns_name=columns_name,columns_type=columns_type,initial_name=initial_name,user=session['username'])
            else:
                col=get_column_names(table)
                columns_name=[item[0] for item in col]
                columns_type=[item[1] for item in col]
                print(columns_name)
                print(table)
                jig_dict={}
                for name in columns_name:
                    if "Jig" in name:
                        ll=get_table_rows(name)
                        jig_dict[name]=ll
                print(jig_dict)
                if request.method == 'POST':
                    emsg=general_update(request,table,columns_name)
                    if emsg:
                        return render_template('update_element.html',table_name=table,columns_name=jsonify(columns_name),columns_type=columns_type,initial_name=initial_name,jig_dict=jig_dict,error_message=emsg,user=session['username'])            
                    else:
                        return redirect('/update/'+table+'/'+request.form["new_serial"])
                else:
                    return render_template('update_element.html', table_name=table, columns_name=columns_name,columns_type=columns_type, initial_name=initial_name, jig_dict=jig_dict,user=session['username'])
    else:
        return redirect('login')

@app.route('/QC')
def qcMenu():
    if 'loggedin' in session:
        return render_template('qc_menu.html',user=session['username'])
    else:
        return redirect('login')


@app.route('/manage')
def manageMenu():
    if 'loggedin' in session:
        return render_template('manage_assembly.html',user=session['username'])
    else:
        return redirect('login')


@app.route('/manage/<jig>', methods=['GET', 'POST'])
def jigManagemet(jig):
    if 'loggedin' in session:
        col=get_column_names("Jig_"+jig)
        columns_name=[item[0] for item in col]
        columns_type=[item[1] for item in col]
        print(columns_name)
        jig_list=get_table_rows("Jig_"+jig)
        obj_dict={}
        for name in columns_name:
            #selection=filterMap[jig][name]
            if name=="PS_FEH_L":
                selection=filterMap[jig][name]+" AND Side='Left'"
                ll=get_table_rows_filtered("PS_FEH",selection)
                obj_dict[name]=ll
            elif name=="PS_FEH_R":
                selection=filterMap[jig][name]+" AND Side='Right'"
                ll=get_table_rows_filtered("PS_FEH",selection)
                obj_dict[name]=ll
            elif name!="Serial" and "Batch" not in name:
                selection=filterMap[jig][name]
                ll=get_table_rows_filtered(name,selection)
                obj_dict[name]=ll
        print(obj_dict)
        #mechanicalParts
        mp_dict={}
        mp_name=""
        if "inserts" in jig:
            mp_name="Inserts"
        elif "isolator" in jig:
            mp_name="Isolators"
        elif "hvtail" in jig:
            mp_name="HVtails"
        elif "sandwich" in jig:
            mp_name="Spacers"
        if mp_name:
            cName,mp_list=mp_table_data(mp_name)
            print(mp_name)
            print(mp_list)
        else:
            mp_list=None
        if request.method == 'POST':
            print(request.form)
            manage_assembly(jig,columns_name,request)
            return redirect('/manage/'+jig)
        else:
            return render_template('jigs_management.html',jig=jig,jig_list=jig_list,obj_dict=obj_dict,mp_name=mp_name,mp_list=mp_list,user=session['username'])
    else:
        return redirect('login')
    
@app.route('/attachCables', methods=['GET', 'POST'])
def attachCables():
    if 'loggedin' in session:
        obj_dict={}
        name="PS_Module"
        ll=get_table_rows_filtered(name,filterMap["AttachCables"][name])
        obj_dict[name]=ll
        name="VTRX"
        ll=get_table_rows_filtered(name,filterMap["AttachCables"][name])
        obj_dict[name]=ll
        print(obj_dict)
        #mechanicalParts
        mp_name="PowerTails"
        if mp_name:
            cName,mp_list=mp_table_data(mp_name)
            print(mp_name)
            print(mp_list)
        else:
            mp_list=None
        if request.method == 'POST':
            print(request.form)
            manage_assembly("","",request)
            return redirect('/attachCables')
        else:
            return render_template('attach_cables.html',obj_dict=obj_dict,mp_name=mp_name,mp_list=mp_list,user=session['username'])
    else:
        return redirect('login')

@app.route('/attachPins', methods=['GET', 'POST'])
def attachPins():
    if 'loggedin' in session:
        obj_dict={}
        name="PS_Module"
        ll=get_table_rows_filtered(name,filterMap["AttachPins"][name])
        obj_dict[name]=ll
        print(obj_dict)
        #mechanicalParts
        mp_name=["Pins","Fasteners"]
        cName,pins_list=mp_table_data("Pins")
        cName,fast_list=mp_table_data("Fasteners")
        if request.method == 'POST':
            print(request.form)
            manage_assembly("","",request)
            return redirect('/attachPins')
        else:
            return render_template('attach_pins.html',obj_dict=obj_dict,pins_list=pins_list,fast_list=fast_list,user=session['username'])
    else:
        return redirect('login')

@app.route('/upload', methods=['POST'])
def upload_file():
    if 'file' not in request.files:
        return jsonify({'message': 'No file part'}), 400
    
    file = request.files['file']
    name = request.form['name']
    if file.filename == '':
        return jsonify({'message': 'No selected file'}), 400

    if file:
        dir=os.path.join("static/data",name)
        if not os.path.exists(dir):
            os.mkdir(dir)
        filename = file.filename
        file.save(os.path.join(dir, filename))
        return jsonify({'message': 'File uploaded successfully', 'filename': filename}), 200


@app.route('/DBupload')
def DBupload():
    if 'loggedin' in session:
        mytab = request.args.get('TAB', 'mod')
        expectedCache=".session.cache"
        print(expectedCache)
        DBinit=False
        if os.path.isfile(expectedCache):
            t=os.path.getmtime(expectedCache)
            print(t)
            print((time.time()-t)/3600)
            if (time.time()-t)/3600<24:
                DBinit=True
            else:
                os.remove(expectedCache)
                print("cache file removed, re-initialize")
        else:
            print("no cache file")
        cols, rows=fetch_table_data("PS_Module")
        mod=[]
        pss=[]
        mapsa=[]
        for element in rows:
            mod.append((element[cols.index("Name")],"Metrology",element[cols.index("Metrology_Data")],element[cols.index("MetrologyOnDB")]))
            mod.append((element[cols.index("Name")],"PullTest",element[cols.index("PullTestData")],element[cols.index("PullTestOnDB")]))
            mod.append((element[cols.index("Name")],"MissingBond",element[cols.index("MissingBond")],element[cols.index("MissingBondOnDB")]))
            mod.append((element[cols.index("Name")],"IV Curve",element[cols.index("ModuleIV")],element[cols.index("IVonDB")]))
        cols,rows=fetch_table_data("PS_Strip")
        for element in rows:
            pss.append((element[cols.index("Serial")],"Dicing",element[cols.index("Dicing_Data")],element[cols.index("DicingOnDB")]))
            pss.append((element[cols.index("Serial")],"IV Curve",element[cols.index("IV_Data")],element[cols.index("IVonDB")]))
        cols,rows=fetch_table_data("PS_MAPSA")
        for element in rows:
            mapsa.append((element[cols.index("Serial")],"Dicing",element[cols.index("Dicing_Data")],element[cols.index("DicingOnDB")]))
        return render_template('dbupload.html',DBinit=DBinit,cacheFile=expectedCache,modules=mod,pss=pss,mapsa=mapsa,TAB=mytab,user=session['username'])
    else:
        return redirect('login')

@app.route('/DB_connection', methods=['POST'])
def DB_connection():
    if 'loggedin' in session: 
        data = request.json
        u = data.get('user')
        p = data.get('pwd')
        type=data.get('type')
        name=data.get('name') 
        fname=data.get('fname')
        if fname=="None" or fname=="":
            return jsonify({'success': False,'output': 'No data file to be uploaded'})
        bash_script_path='py4dbupload/bin/setup.sh'
        process = subprocess.Popen(['bash', '-c', f'source {bash_script_path} && env'],
                             stdout=subprocess.PIPE,
                             stderr=subprocess.PIPE)
        stdout, stderr = process.communicate()
        #print(stdout)
        print(type+": "+fname)

        # Parse environment variables from the output..needed inside conda
        env_vars = dict(line.strip().split('=', 1) for line in stdout.decode().splitlines())


        system_python_path = '/usr/bin/python3'
        # Path to the script you want to run outside the Conda environment
        if "moduleIV" in type:
            table="PS_Module"
            column="IVonDB"
            script_path = 'py4dbupload/run/uploadOTModuleIV.py  --data "static/data/'+name+'/'+fname+'" --upload'
        elif "moduleMetrology" in type:
            table="PS_Module"
            column="MetrologyOnDB"
            script_path = 'py4dbupload/run/uploadOTModuleMetrology.py  --data "static/data/'+name+'/'+fname+'" --upload'
        elif "modulePullTest" in type:
            table="PS_Module"
            column="PullTestOnDB"
            script_path = 'py4dbupload/run/uploadOTWireBondPullTest.py  --data "static/data/'+name+'/'+fname+'" --upload'
        elif "moduleMissingBond" in type:
            table="PS_Module"
            column="MissingBondOnDB"
            script_path = 'py4dbupload/run/uploadOTMissingBond.py  --data "static/data/'+name+'/'+fname+'" --upload'
        elif "stripIV" in type:
            table="PS_Strip"
            column="IVonDB"
            script_path = 'py4dbupload/run/uploadOTSensorIV.py  --data "static/data/'+name+'/'+fname+'" --upload'
        elif "Dicing" in type:
            if "mapsa" in type:
                table="PS_MAPSA"
            else:
                table="PS_Strip"
            column="DicingOnDB"
            script_path = 'py4dbupload/run/uploadOTSensorMetrology.py  --data "static/data/'+name+'/'+fname+'" --upload'
        else:
            return jsonify({'success': False,'output': 'Data to upload not recognized'})                                                                                                                                                   
            #script_path = 'py4dbupload/run/query.py  --locations'
        
        print(f'{system_python_path} {script_path}')
        # Start the external script using pexpect
        child = pexpect.spawn(f'{system_python_path} {script_path}',env=env_vars)
        responses = []

        while True:
            try:
                # List of expected prompts
                prompts = [
                    "Username:",
                    "Password:",
                    pexpect.EOF,
                    pexpect.TIMEOUT
                ]

                index = child.expect(prompts,timeout=10)

                if index == 0:
                    print("user")
                    child.sendline(u)
                elif index == 1:
                    print("pwd")
                    child.sendline(p)
                elif index == 2:
                    print("External script completed.")
                    break
                elif index == 3:
                    # Timeout occurred, no more prompts                                                                                                                                                                      
                    print("No more prompts, exiting.")
                    break
            except pexpect.exceptions.EOF:
                # End of file reached unexpectedly                                                                                                                                                                             
                responses.append("Unexpected EOF encountered.")
                return jsonify({'output': responses})
            except pexpect.exceptions.TIMEOUT:
                # Timeout reached unexpectedly                                                                                                                                                                                 
                responses.append("Timeout encountered.")
                return jsonify({'output': responses})

        outp=child.before.decode().strip()
        print(outp)
        responses.append(outp)
        child.close()
        if "Response code: 200" in outp:
            uploadStatus=dbFlag_update(table,column,name)
            if uploadStatus:
                return jsonify({'success': True,'output': 'Successfull uploading'})
            else:
                return jsonify({'success': False,'output': 'Failed to change Local DB flag!'})    
        else:
            return jsonify({'success': False,'output': responses})

    else:
        return redirect('login')

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=8888, debug=True)
    #socketio.run(app,host='0.0.0.0', port=8888, debug=True)
